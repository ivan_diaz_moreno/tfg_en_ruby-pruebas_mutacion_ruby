# Contains the path of the project
BASE_PATH = File.dirname(__FILE__)
# Contains the path of the ini_operators_files in the project
OPERATORS_FILE_PATH = File.expand_path(BASE_PATH, 'ini_operators_files/')
MUTATIONS_FILE_PATH_SAVE = BASE_PATH + '/generate_mutations/'
TESTS_RESULTS_FILE_PATH_SAVE = BASE_PATH + '/tests_results/'
