require_relative 'operators_extra_checks'

class RewriteTree < Parser::TreeRewriter
  include ExtraChecks

  def replace_current_operators(current_operators)
    @@operators_to_apply = current_operators
    @@is_lvar_valid = false
    @@is_int_valid = false
  end

  def on_send(node)
    # Check if it is a replace operation and if the actual operator match with the operator in the
    # operation
    operation_type = @@operators_to_apply['type']
    actual_op = @@operators_to_apply['operator_to_replace']
    if node.loc.selector.is?(actual_op)
      if operation_type == "R" || operation_type == "D" || operation_type == "R_U"
        # Evaluate if is a freeze delete operator
        if actual_op == "freeze"
          # In freeze delete we need to delete the dot too
          replace(node.loc.dot, @@operators_to_apply['new_operator'])
        end
        replace(node.loc.selector, @@operators_to_apply['new_operator'])
      end
    end
    # Check if is a class checker replace
    if actual_op == "sim_instance_check"
      if node.loc.selector.is?('instance_of?')
        replace(node.loc.selector, 'is_a?')
      end
    elsif actual_op == "comp_instance_check"
      if node.loc.selector.is?('is_a?') || node.loc.selector.is?('kind_of?')
        replace(node.loc.selector, 'instance_of?')
      end
    end
    # Check if is a index replace
    if ExtraChecks.evaluate_if_is_a_index_and_index_replace_operation(node.loc.expression.source, @@operators_to_apply)
      # Only the lvar and int negative in index replace came in a send, so
      # check if the index replace is to replace - with +
      if actual_op == '-i'
        # If is a negative replace, change the - with ''
        new_content = node.loc.selector.source.sub('-', '')
        replace(node.loc.selector, new_content)
      end
    end
    if operation_type == "A"
      # The ! aggregation need extra checks
      if @@operators_to_apply['new_operator'] == "!"
        if ExtraChecks.extra_simple_check_add_negation(node.loc.selector, @@operators_to_apply, "!")
          wrap(node.loc.selector, @@operators_to_apply['new_operator'], "")
          #The source take the content of selector in str mode
        elsif ExtraChecks.evaluate_if_is_a_relational_operator(node.loc.selector.source)
          wrap(node.loc.expression, @@operators_to_apply["new_operator"]+"(", ")")
        end
      elsif @@operators_to_apply['new_operator'] == "~"
        if ExtraChecks.evaluate_if_is_a_logical_operator(node.loc.selector)
          wrap(node.loc.expression, @@operators_to_apply["new_operator"]+"(", ")")
        elsif ExtraChecks.extra_simple_check_add_negation(node.loc.selector, @@operators_to_apply, "~")
          wrap(node.loc.selector, @@operators_to_apply['new_operator'], "")
        end
      end
    end
  end

  def on_if(node)
  end

  def on_and(node)
    if @@operators_to_apply['type'] == "R" || @@operators_to_apply['type'] == "R_U"
      return replace_and_or(node, "and", "&&")
    end
    if @@operators_to_apply['type'] == "A"
      if ExtraChecks.evaluate_if_is_add_negation_operation(@@operators_to_apply)
        wrap(node.loc.expression, @@operators_to_apply["new_operator"]+"(", ")")
      end
    end
  end

  def on_or(node)
    if @@operators_to_apply['type'] == "R" || @@operators_to_apply['type'] == "R_U"
      return replace_and_or(node, "or", "||")
    end
    if @@operators_to_apply['type'] == "A"
      if ExtraChecks.evaluate_if_is_add_negation_operation(@@operators_to_apply)
        wrap(node.loc.expression, @@operators_to_apply["new_operator"]+"(", ")")
      end
    end
  end

  def on_while(node)
  end

  def on_until(node)
  end

  def on_for(node)
  end

  def on_irange(node)
    if @@operators_to_apply['type'] == "R"
      # Check if is a replace of .. with ...
      if @@operators_to_apply['operator_to_replace'] == ".." && @@operators_to_apply['new_operator'] == '...'
        replace(node.loc.operator, @@operators_to_apply['new_operator'])
      end
    end
  end

  def on_erange(node)
    if @@operators_to_apply['type'] == "R"
      # Check if is a replace of .. with ...
      if @@operators_to_apply['operator_to_replace'] == "..." && @@operators_to_apply['new_operator'] == '..'
        replace(node.loc.operator, @@operators_to_apply['new_operator'])
      end
    end
  end

  def on_block(node)
  end

  def on_op_asgn(node)
    # This function treats the nodes of type op-as an example is a += 2.
    # Given a node it checks if the operator to replace is in it. If so, it is changed by the new operator.
    # It does two types of checks, if the operator is in an expression of type 'x =' or
    # if the operator is 'x =' and replaces it with the desired operator.
    # :param node: the node to check
    # :return: a Rewriter obj with the modifications
    actual_assign_operator = node.loc.operator.source
    op_to_replace = @@operators_to_apply['operator_to_replace']
    new_op = @@operators_to_apply['new_operator']
    # Check if it is a replace operation
    if @@operators_to_apply['type'] == "R"
      # First check. Looks if the operator is in the expression
      reg_exp = Regexp.escape("#{op_to_replace}=")
      if actual_assign_operator.match(reg_exp)
        replace(node.loc.operator, "#{new_op}=")
        # Second check. Looks if the operator is the expression
      elsif node.loc.operator.is?(op_to_replace)
        replace(node.loc.operator, new_op)
      end
    end
  end

  def on_while_post(node)
  end

  def on_until_post(node)
  end

  def on_kwbegin(node)
  end

  def on_return(node)
    # Check if the operation is a return delete
    if node.loc.keyword.is?(@@operators_to_apply['operator_to_replace']) && @@operators_to_apply['type'] == 'D'
      replace(node.loc.keyword, @@operators_to_apply['new_operator'])
    end
  end

  def on_begin(node)
  end

  def on_lvasgn(node)
    # if is a operator created by user
    if @@operators_to_apply['type'] == 'R_U'
      if node.loc.name.is?(@@operators_to_apply['operator_to_replace'])
        replace(node.loc.name, @@operators_to_apply['new_operator'])
      end
    end
  end

  def on_lvar(node)
    if @@is_lvar_valid
      if @@operators_to_apply['type'] == 'A'
        insert_before(node.loc.expression,@@operators_to_apply['new_operator'])
      elsif @@operators_to_apply['type'] == 'R'
        # If is a index replace with + to -
        if @@operators_to_apply['operator_to_replace'] == "+i"
          node_content = node.loc.expression.source
          if node_content.match('\+.*') || node_content.match('^[^-].*')
            node_content = node.loc.expression.source.sub('+', '')
            replace(node.loc.expression, @@operators_to_apply['new_operator'] + node_content)
          end
        end
      end
      # Else if a operator created by user
    elsif @@operators_to_apply['type'] == 'R_U'
      if node.loc.expression.is?(@@operators_to_apply['operator_to_replace'])
        replace(node.loc.expression, @@operators_to_apply['new_operator'])
      end
    end
  end

  def on_int(node)
    if @@is_int_valid
      if @@operators_to_apply['type'] == 'R'
        # If is a index replace with + to -
        if @@operators_to_apply['operator_to_replace'] == "+i"
          node_content = node.loc.expression.source
          if node_content.match('\+.*') || node_content.match('^[^-].*')
            node_content = node.loc.expression.source.sub('+', '')
            replace(node.loc.expression, @@operators_to_apply['new_operator'] + node_content)
          end
        end
      end
      # Else if a operator created by user
    elsif @@operators_to_apply['type'] == 'R_U'
      if node.loc.expression.is?(@@operators_to_apply['operator_to_replace'])
        replace(node.loc.expression, @@operators_to_apply['new_operator'])
      end
    end
  end

  def on_masgn(node)
    if @@operators_to_apply['type'] == 'R'
      # If is an assigment balance operation
      if @@operators_to_apply['operator_to_replace'] == "balance"
        actual_exp = node.loc.expression
        exp_balance =ExtraChecks.multiple_assignment_balance(actual_exp.source)
        replace(actual_exp, exp_balance)
      end
    end
  end

  def on_str(node)    # If is a operator created by user
    if @@operators_to_apply['type'] == 'R_U'
      # Delete the quotes from the str expression in the node
      str_without_quotes = node.loc.expression.source.gsub(/\"/, "").gsub(/\'/, "")
      if str_without_quotes == @@operators_to_apply['operator_to_replace']
        # If expression came with "", then need to write the new content with "", else no
        if node.loc.expression.source.match("\".*\"")
          replace(node.loc.expression, "\"" + @@operators_to_apply['new_operator'] + "\"")
        elsif node.loc.expression.source.match("'.*'")
        replace(node.loc.expression, "'" + @@operators_to_apply['new_operator'] + "'")
        else
          replace(node.loc.expression, @@operators_to_apply['new_operator'])
        end
      end
    else
      # Check if is a quote replace and in affirmative case make the replace
      str_quote_replace(node)
    end
  end

  def on_dstr(node)
    # This type of node contains the str with vars in like "hola #{var_1}"

    # Check if is a quote replace and in affirmative case make the replace
    str_quote_replace(node)
  end

  def on_def(node)
  end

  def on_args(node)
  end

  def on_arg(node)
    # If is a operator created by user
    if @@operators_to_apply['type'] == 'R_U'
      if node.loc.expression.is?(@@operators_to_apply['operator_to_replace'])
        replace(node.loc.expression, @@operators_to_apply['new_operator'])
      end
    end
  end

  def on_cvasgn(node)
    # This type of node is the node for class variables
      if @@operators_to_apply['type'] == 'R_U'
        if node.loc.name.is?(@@operators_to_apply['operator_to_replace'])
          replace(node.loc.name, @@operators_to_apply['new_operator'])
        end
      end
  end

  def on_ivasgn(node)
    # This type of node is the node for instance variables
    if @@operators_to_apply['type'] == 'R_U'
      if node.loc.name.is?(@@operators_to_apply['operator_to_replace'])
        replace(node.loc.name, @@operators_to_apply['new_operator'])
      end
    end
  end

  def on_array(node)
  end

  def on_hash(node)
  end

  def on_pair(node)
  end

  def str_quote_replace(node)
    # Check if is a quote replace and in affirmative case make the replace
    # :param node: the node to evaluate
    # :return: the node changes or nothing
    actual_op = @@operators_to_apply['operator_to_replace']
    # If is a quote replace operation
    if @@operators_to_apply['type'] == 'R' && actual_op.match('.*_q')
      # Check if has begin and end element. The str with vars in, doesnt
      # have end and begin and its type is dstr
      if node.loc.begin && node.loc.end
        if actual_op == 'sin_q' && node.loc.begin.is?('\'')
          replace(node.loc.begin, "\"")
          return replace(node.loc.end, "\"")
        elsif actual_op == 'dou_q' && node.loc.begin.is?("\"")
          replace(node.loc.begin, '\'')
          return replace(node.loc.end, '\'')
        end
      end
    end
  end

  def replace_and_or(node, operator_write_1, operator_write_2)
    # First see if the operator is the same, if it is not then evaluate if the current operator
    # is the one passed in text1 (or || and) and evaluate if the operator to be replaced
    # is (|| or &&), if they match then make the change.
    # :param node: the node to evaluate the operator.
    # :param operator_write_1: the literal form of the operator function caller. Ex: 'or'
    # :param operator_write_2: the no literal form of the operator function caller. Ex: '||'
    # :return: the string with the mutation
    operator_to_replace = @@operators_to_apply['operator_to_replace']
    new_operator = @@operators_to_apply['new_operator']
    # Check if it is a replace operation
    if @@operators_to_apply['type'] == "R" || @@operators_to_apply['type'] == "R_U"
      if node.loc.operator.is?(operator_to_replace)
        return replace(node.loc.operator, new_operator)
      elsif node.loc.operator.is?(operator_write_1) && operator_to_replace == operator_write_2
        return replace(node.loc.operator, new_operator)
      elsif node.loc.operator.is?(operator_write_2) && operator_to_replace == operator_write_1
        return replace(node.loc.operator, new_operator)
      end
    end
  end

  def set_is_lvar_valid(status)
    # Save if the next node with type lvar is valid to apply the changes
    # :param status: true if the next lvar is a valid node to apply changes
    # or false if not
    # :return: None
    @@is_lvar_valid = status
  end

  def set_is_int_valid(status)
    # Save if the next node with type int is valid to apply the changes
    # :param status: true if the next int is a valid node to apply changes
    # or false if not
    # :return: None
    @@is_int_valid = status
  end

end
