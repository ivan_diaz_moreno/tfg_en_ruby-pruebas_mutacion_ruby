require "parser/current" # Import parser content
require_relative '../file_dir_manager/file_and_directory_manager' #Import the manager of files and dirs

# This module contains all the functions to transform ruby content files into ast objects
module TransformerModule
  include FileAndDirectoryManager
  @content = -1 # Var which will contain the content of the read ruby file
  @ast = -1 # Var which will contain the ast generate from the read ruby file

  def read_file(file_path)
    # Read the file given in the param and save its content in the module's var
    # :param file_path: str which contain the path of the file
    # :return: None
    if FileAndDirectoryManager.check_file_is_ruby(file_path) == true
      FileAndDirectoryManager.with_open File, file_path, 'r' do |f|
        @content = f.read
      end
    else
      raise StandardError, 'Archivo con terminacion no valida. Se necesita archivo .rb para mutar', caller
    end
  end

  def generate_ast_from_content_file
    # Given the content of a read file saved in @content transform it into ast
    # :return: ast of the code and the buffer with the source code information
    # or nil if the content has errors or there are not content
    if @content != -1
      begin
        @ast = Parser::CurrentRuby.parse(@content)
      rescue Exception => error
        print "ERROR -> no se puede generar mutante ya que el codigo proporcionado contiene errores: ", error, "\n"
      else
        # A buffer with source code. Buffer contains the source code itself,
        # associated location information (name and first line), and takes care of encoding.
        # A source buffer is immutable once populated.
        code_source_buffer = Parser::Source::Buffer.new('buffer')
        code_source_buffer.source = @content
        return @ast, code_source_buffer
      end
    else
      puts 'No se ha realizado transformacion ya que no hay contenido leido'
    end
    # If the content has errors or there are not content returns nil
    nil
  end

  def print_ast
    # REVIEW
    @ast
  end

  module_function :read_file, :generate_ast_from_content_file, :print_ast

end
