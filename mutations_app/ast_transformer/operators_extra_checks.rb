module ExtraChecks

  def extra_simple_check_add_negation(node_operator,operators_to_apply, negation_type)
    # Extra checker to operation add ! and ~. Is needed to know if the actual node
    # is "" or contains an other negation. In this two cases , the ! or ~ op
    # could be aggregated to the node
    # :param node_operator: node operator to evaluate
    # :param operators: dict with the actual operator to search and replace
    # :param negation_type: str with the type of negation. logical -> "~"
    # or conditional -> "!"
    # :return: true if match or false if not

    # If the actual operator is "!" or "" is possible to add a new '!'
    if node_operator.is?(operators_to_apply['operator_to_replace'])
      return true
    elsif node_operator.is?(operators_to_apply['new_operator'])
      return true
    end
    return false
  end

  def check_relational_operators_in_add_negation(node_operator)
    # Check if node contains an accepted relational operator
    # A negation could apply to the operands in the != and == relational
    # operations
    # :param node_operator: node to evaluate (must be send type)
    # :return: true if match or false if not
    if node_operator.is?("!=") || node_operator.is?("==")
      return true
    end
    return false
  end

  def check_type_of_node(node, ast_rewriter, code_source_buffer, operators)
    # Depends of the node type, some nodes need extra operation
    # depending on the operators that may have inside. This func
    # evaluate if the node is in this special cases
    # :param node: node to evaluate
    # :param ast_rewriter: a object of the type RewriteTree to call the correct rewrite
    # :param code_source_buffer: A buffer with source code.
    # :param operators: dict with the actual operator to search and replace
    # :return: array with the mutations code generated or empty array if there are
    # not mutations
    mutations_code = Array.new
    if node.type == :send
      # Check if node contains relational operation
      if evaluate_if_is_add_negation_operation(operators)
        if check_relational_operators_in_add_negation(node.loc.selector)
          mutations_code = apply_adding_operations_only_lvar(node, ast_rewriter, code_source_buffer, mutations_code)
        end
      elsif evaluate_if_is_add_logical_negation_operation(operators)
        if evaluate_if_is_a_logical_operator(node.loc.selector)
          mutations_code = apply_adding_operations_only_lvar(node, ast_rewriter, code_source_buffer, mutations_code)
        end
      elsif evaluate_if_is_a_index_and_index_replace_operation(node.loc.expression.source, operators)
        mutations_code = apply_replace_operations_lvar_and_int(node, ast_rewriter, code_source_buffer, mutations_code, operators)
      end
    elsif node.type == :and
      if evaluate_if_is_add_negation_operation(operators)
        mutations_code = apply_adding_operations_only_lvar(node, ast_rewriter, code_source_buffer, mutations_code)
      end
    elsif node.type == :or
      if evaluate_if_is_add_negation_operation(operators)
        mutations_code = apply_adding_operations_only_lvar(node, ast_rewriter, code_source_buffer, mutations_code)
      end
    elsif node.type == :if
      if evaluate_if_is_add_negation_operation(operators)
        mutations_code = apply_adding_operations_only_lvar(node, ast_rewriter, code_source_buffer, mutations_code)
      end
    elsif node.type == :while || node.type == :while_post
      if evaluate_if_is_add_negation_operation(operators)
        mutations_code = apply_adding_operations_only_lvar(node, ast_rewriter, code_source_buffer, mutations_code)
      end
    elsif node.type == :until || node.type == :until_post
      if evaluate_if_is_add_negation_operation(operators)
        mutations_code = apply_adding_operations_only_lvar(node, ast_rewriter, code_source_buffer, mutations_code)
      end
    end
    return mutations_code
  end

  def evaluate_if_is_a_index_and_index_replace_operation(node_content, operators)
    # Check if the actual node content is an index access like a[x] and
    # look if the actual operator is a replace index sign
    # :param node_content: str with node content to evaluate
    # :param operators: dict with the actual operator to search and replace
    # :return: true if match or false if not
    if operators['type'] == "R"
      if operators['operator_to_replace'] == "+i"
        if node_content.match('.+\[+.*\].*') || node_content.match('.+\[[^-].*\].*')
          return true
        end
      elsif operators['operator_to_replace'] == "-i"
        if node_content.match('.+\[-.*\].*')
          return true
        end
      else
        return false
      end
    end
  end

  def evaluate_if_is_add_negation_operation(operators)
    # Check if the operation type is A and the new operator is "!"
    # :param operators: dict with the actual operator to search and replace
    # :return: true if match or false if not
    if operators['new_operator'] == "!" and operators['type'] == "A"
      return true
    else
      return false
    end
  end

  def evaluate_if_is_add_logical_negation_operation(operators)
    # Check if the operation type is A and the new operator is "~"
    # :param operators: dict with the actual operator to search and replace
    # :return: true if match or false if not
    if operators['new_operator'] == "~" and operators['type'] == "A"
      return true
    else
      return false
    end
  end

  def evaluate_if_is_a_relational_operator(operator)
    # Given a operator check, if is a relational operator
    # :param operator: the str with the operator
    # :return: true if match or false if not
    if operator == ">"||operator == ">="||operator == "<"||operator == "<="||operator == "=="||operator == "!="
      return true
    else
      return false
    end
  end

  def evaluate_if_is_a_logical_operator(node_operator)
    # Given a operator, check if is a logical operator
    # :param node_operator:  the operator to evaluate.
    # :return: true if match or false if not
    operator = node_operator
    if operator.is?("&")||operator.is?("|")||operator.is?("^")||operator.is?(">>")||operator.is?("<<")
      return true
    else
      return false
    end
  end

  def evaluate_if_is_a_index_sign_replace(operators)
    # Given a operator, check if is a index replace operator
    # :param operators:  the operator to evaluate.
    # :return: true if match or false if not
    if operators['operator_to_replace'] == "+i" || operators['operator_to_replace'] == "-i"
      return true
    else
      return false
    end
  end

  def apply_adding_operations_only_lvar(node, ast_rewriter, code_source_buffer, mutations_code)
    # When a node has subnodes to which the aggregation of some operand can be applied,
    # they need to call this function.
    # This takes care of applying the aggregation to the correct subnodes.
    # :param node: node to evaluate
    # :param ast_rewriter: a object of the type RewriteTree to call the correct rewrite
    # :param code_source_buffer: A buffer with source code.
    # :param mutations_code: array to save the generate mutations
    # :return: array with the mutations code generated or empty array if there are
    # not mutations
    node.children.each { |node_ch|
      if node_ch.is_a?(Parser::AST::Node)
        if node_ch.type == :lvar
          # Tell the rewriter that the next lvar node is part of a
          # relational operation
          ast_rewriter.set_is_lvar_valid(true)
          code_mutation = ast_rewriter.rewrite(code_source_buffer, node_ch)
          mutations_code.append(code_mutation)
          # Tell the rewriter that the next lvar node isn't part of a
          # relational operation
          ast_rewriter.set_is_lvar_valid(false)
        end
      end
    }
    return mutations_code
  end

  def apply_replace_operations_lvar_and_int(node, ast_rewriter, code_source_buffer, mutations_code, operators)
    # When a node has subnodes to which the replacement of some operand can be applied,
    # they need to call this function.
    # This takes care of applying the replace to the correct subnodes.
    # :param node: node to evaluate
    # :param ast_rewriter: a object of the type RewriteTree to call the correct rewrite
    # :param code_source_buffer: A buffer with source code.
    # :param mutations_code: array to save the generate mutations
    # :return: array with the mutations code generated or empty array if there are
    # not mutations
    num_of_lvar = 0
    node.children.each { |node_ch|
      if node_ch.is_a?(Parser::AST::Node)
        if node_ch.type == :lvar and num_of_lvar == 0
          num_of_lvar += 1
        elsif node_ch.type == :lvar
          # Tell the rewriter that the next lvar node is valid
          ast_rewriter.set_is_lvar_valid(true)
          code_mutation = ast_rewriter.rewrite(code_source_buffer, node_ch)
          mutations_code.append(code_mutation)
          # Tell the rewriter that the next lvar node isn't valid
          ast_rewriter.set_is_lvar_valid(false)
          num_of_lvar += 1
          if evaluate_if_is_a_index_sign_replace(operators)
            break
          end
        elsif node_ch.type == :send
          # The lvar with - came in a send node that make the
          # replace operator in other place, but we need to
          # count it like a lvar to avoid later the not valid ints
          if evaluate_if_is_a_index_sign_replace(operators)
            code_mutation = ast_rewriter.rewrite(code_source_buffer, node_ch)
            mutations_code.append(code_mutation)
            num_of_lvar += 1
          end
        elsif node_ch.type == :int
          # If there are 2 lvar nodes before the int, that means that the element in
          # the index is a lvar, so the next ints are not valid to execute the replace.
          # Ex: a[b] = 1 -> in this case the 1 is not valid int because we have to lvar
          # before and one of this, the b, is the index
          if evaluate_if_is_a_index_sign_replace(operators) && num_of_lvar >= 2
            break
          end
          # Tell the rewriter that the next int node is valid
          ast_rewriter.set_is_int_valid(true)
          code_mutation = ast_rewriter.rewrite(code_source_buffer, node_ch)
          mutations_code.append(code_mutation)
          # Tell the rewriter that the next int node isn't valid
          ast_rewriter.set_is_int_valid(false)
          if evaluate_if_is_a_index_sign_replace(operators)
            break
          end
        end
      end
    }
    return mutations_code
  end

  def multiple_assignment_balance(expression)
    # Check that the expression passed by parameter is a balanced
    # multiple assignment. In case of not being it balances it.
    # :param expression: str with the expression to evaluate
    # :return: the generate expression balance
    in_assigned, receiver, assigned, equal_pos = false, 0, 0, 0
    expression.chars.each do |elem|
      if in_assigned == true && elem != ',' && elem != ' ' # Elements after equal
        assigned += 1
      elsif elem != '=' && elem != ',' && elem != ' '  # Elements before equal
        receiver += 1
      elsif elem == '='
        equal_pos = expression.index(elem)
        in_assigned = true
      end
    end
    while receiver != assigned
      if receiver > assigned
        # equal_pos-2 because equal_pos is the pos of the '='
        # and the equal_pos-1 is the ' '
        expression.slice!(equal_pos-2) # Delete the element
        expression.slice!(equal_pos-3) # Delete the ','
        equal_pos -= 2
        receiver -= 1
      elsif receiver < assigned
        expression = expression.chop # Delete the element
        expression = expression.chop # Delete the ','
        assigned -= 1
      end
    end
    return expression
  end

  module_function :extra_simple_check_add_negation, :check_relational_operators_in_add_negation, :check_type_of_node,
                  :evaluate_if_is_add_negation_operation, :apply_adding_operations_only_lvar,
                  :evaluate_if_is_a_relational_operator, :evaluate_if_is_add_logical_negation_operation,
                  :evaluate_if_is_a_logical_operator, :evaluate_if_is_a_index_and_index_replace_operation,
                  :apply_replace_operations_lvar_and_int, :evaluate_if_is_a_index_sign_replace,
                  :multiple_assignment_balance
end
