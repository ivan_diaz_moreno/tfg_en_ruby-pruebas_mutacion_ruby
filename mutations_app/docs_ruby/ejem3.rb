# Fichero de ejemplo para comprobar funcionamiento en bucles


def loops

  # Case 1
  i = 0
  while i < 10
    i += 1
  end

  # Case 2
  i = 0
  begin
    i += 1
  end while i < 10

  # Case 3
  i = 0
  until i >= 10
    i += 1
  end

  # Case 4
  i = 0
  begin
    i += 1
  end until i >= 10

  # Case 5
  for i in 1..10
    puts i
  end

  # Case 6
  for i in [1, 2, 3]
    puts i
  end

  # Case 7
  (1..10).each { |i|
    puts i
  }


end
