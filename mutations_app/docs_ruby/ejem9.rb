# Fichero de ejemplo para probar la agregacion de freeze

class Example
  def initialize
    @example = 1
  end
  def set_example(a)
    @example = a
  end

end

def add_freeze(a)
  a = [1,2,3].freeze
  a << 2

  b = Hash.new()
  b.freeze

  c = Example.new.freeze
  c.set_example(2)

  d = {'r' => 'hola'}.freeze
end

add_freeze(4)
