# Fichero de ejemplo para probar operadores de inserción y borrado

class ClassForATry
  @@stop = 1
  @@hi = "hola"
  def set_sum(a)
    @sum_value = a
  end
end

def fun_b(a)
  return a
end

def relational_and_conditional(a,b)
  if !b
    if a and b
      b = fun_b(b)
    elsif a
      a = 2
    end
  end

  while a && b
    if !a != b or a >= b
      a + b
    end
  end

  begin
    i += 1
  end until a

  a = 'holi'
  b = "adios" + 'bye'

  return "hola#{a} vecinito."

end