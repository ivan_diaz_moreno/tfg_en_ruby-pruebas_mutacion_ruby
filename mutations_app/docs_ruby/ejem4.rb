# Fichero de ejemplo para probar operadores relacionales y condicionales

def relational_and_conditional(a,b,c)
  if a >= b && c != b and c > 0
    if c <= a
      return a + b
    elsif c >= a or b == a || b <= c
      return b
    end
  end
end