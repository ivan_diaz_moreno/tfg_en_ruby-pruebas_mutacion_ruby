def sum(numero)
  # Seccion 1 de pruebas para condicionales
  if numero < 0
    es = "negativo"
  elsif numero == 0
    if numero < 1
      es = "cero"
    end
  else
    unless numero <= 0
      es = "negativo"
    else
      es = "mayor o igual que 0"
    end
  end
  # Seccion 2 de pruebas para condicionales
  es = if numero < 0
         "negativo"
       elsif numero == 0
         "cero"
       else
         "p"
       end
  # Seccion 3 de pruebas para condicionales
  if numero < 0 then es = "cero" end
  es = "cero" if numero < 0

  es
end