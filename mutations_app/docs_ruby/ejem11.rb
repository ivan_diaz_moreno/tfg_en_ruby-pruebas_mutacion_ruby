# Fichero para probar el cambio de is_a? y kind_of? por instance_of?

def instance_verification(a)
  if a.is_a?(Array)
    return true
  elsif a.kind_of?(Hash)
    return false
  end

  if a.instance_of?(Set)
    return nil
  end
end
