require_relative '../ejem1'
require 'test/unit'

class TestDeMatesAdvance < Test::Unit::TestCase
  def test_sum_ok_expert
    assert_equal 3000, sum(1000,1000)
  end
  def test_sum_bad_expert
    assert_not_equal 0, sum(1000,2000)
  end
  def test_return_true_expert
    assert_boolean(sum(-1,-2))
  end
end
