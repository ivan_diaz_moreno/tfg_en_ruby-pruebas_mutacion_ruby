require_relative '../ejem1'
require 'test/unit'

class TestDeMates < Test::Unit::TestCase
  def test_sum_ok
    assert_equal 2, sum(1,1)
  end
  def test_sum_bad
    assert_not_equal 3, sum(1,1)
  end
  def test_return_true
    assert_boolean(sum(-1,-1))
  end
end
