def sum(a,b)
  if a < 0 || b < 0
    return true
  end
  c = b + a
  if c > 5
    return c + a
  else
    return c
  end
end