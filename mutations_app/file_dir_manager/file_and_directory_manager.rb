require 'fileutils' # Import tools to work with dirs

module FileAndDirectoryManager
  # This module contains all the implementations required to manage files and dirs

  def FileAndDirectoryManager.with_open(klass, *args)
    #  Context manager function to open files and ensure the close of them
    # :param klass: the type of object, if u want to read a file must be File
    # :param *args: arbitrary arguments, at least it must contain the file path and the mode of open
    # example -> my_path/file.x , 'r'
    yield r = klass.open(*args)
  ensure
    r.close
  end

  def FileAndDirectoryManager.generate_folder(path)
    # Given a route check if it exists, and if it doesn't exist then create it
    # :param path: the path to evaluate and create if doesn't exist
    unless FileAndDirectoryManager.check_if_folder_exits(path)
      FileUtils.mkpath(path)
    end
  end

  def FileAndDirectoryManager.check_if_folder_exits(path)
    # Check if a given folder is in the system.
    # :param path: the path to evaluate.
    # :return: true if exist or false if not.
    if Dir.exist?(path)
      return true
    end
    false
  end

  def FileAndDirectoryManager.check_if_file_exists(path)
    # Check if a given file path is in the system.
    # :param path: the path to evaluate.
    # :return: true if exist or false if not.
    if File.exist?(path)
      return true
    end
    false
  end

  def FileAndDirectoryManager.delete_folder(path)
    # Given a route delete the content of the path and the folder
    # :param path: the path to evaluate and delete
      FileUtils.rm_rf(path)
  end

  def FileAndDirectoryManager.check_file_is_ruby(file_path)
    # Check if the a file has extension .rb
    # :param file_path: str which contain the path of the file
    # :return: true if it is .rb and false in the other case
    if file_path.match(/.*.rb$/)
      true
    else
      false
    end
  end

  def FileAndDirectoryManager.get_file_names_from_folder(path)
    # Generator function, given a path return all the files in the path one by one
    # :param path: str which contain the path of the folder to examine
    # :return: a generator which return the name file by file
    if FileAndDirectoryManager.check_if_folder_exits(path)
      files = Dir.entries(path).select {|f| !File.directory? f} #Skype subdirectories and ".", ".." dotted folders
      generator = Enumerator.new do |enum|
        files.each { |file|
          enum.yield file
        }
      end
      generator
    end
  end

  def FileAndDirectoryManager.delete_file(path)
    # Given a route file delete the file
    # :param path: the path of the file to remove
    File.delete(path) if FileAndDirectoryManager.check_if_file_exists(path)
  end

  def FileAndDirectoryManager.get_line_number(file, word)
    # Given a route file and a word, return the line of the file which contains the word
    # :param file: the path of the file to remove
    # :param word: the word to search
    # :return: the number of the line or null if dont find the word in the file
    # :return: the number of the line or null if dont find the word in the file
    count = 0
    formalize_word = word.strip
    FileAndDirectoryManager.with_open File, file, 'r' do |f|
      f.each_line do |line|
        count += 1
        # Remove the \n elements and the first blank space
        ward = line.gsub("\n", "").strip
        return count if ward == formalize_word
      end
    end
    return nil
  end

end
