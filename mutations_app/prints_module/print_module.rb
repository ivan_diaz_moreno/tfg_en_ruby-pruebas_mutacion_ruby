# frozen_string_literal: true

# This module contains all the necessary functions
# to make the prints in the non-graphic version
# of the program
module Prints
  def no_selection
    # Print function
    'Ninguna operacion ha sido seleccionada'
  end

  def end_of_program
    # Print function
    'Fin de la ejecucion del programa'
  end

  def no_mutations_generate(operator)
    # Print function
    'No se han generado mutaciones para el operador  ' + operator['operator_to_replace'] +
        "  que se queria reemplazar con  " + operator['new_operator']
  end

  def transform_into_ast_error
    # Print function
    'Por favor, solvente los errores en el fichero antes de realizar la mutacion o introduzca otro fichero.

    '
  end

  def original_file_and_test_errors
    puts "\n\n\n\nEn fichero original tiene errores, fallos, pendientes, omisiones o notificaciones al pasar los tests."
    puts "Los tests no se han ejecutado. Por favor solvente los errores antes de volver a ejecutar.\n\n"
  end

  def print_mutations_and_tests_results(results, operator, mutate_info)
    puts "Resultados para las mutaciones generadas con el operador: #{operator['description']}\n\n"
    puts "1) total mutantes: #{results['mutations_status'].length.to_s}"
    puts "2) mutantes vivos: #{results['alive_mutations']}"
    puts "3) mutantes muertos: #{results['dead_mutations']}\n\n"
    puts "4) porcentaje de mutacion: #{results['mutation_percentage']}%\n\n"
    puts "Lista de mutantes y su estado final tras pasar los tests: "
    results['mutations_status'].select { |k,v| puts "Mutacion #{k} en estado: #{v}"}

    puts "\n\nResultado de los tests para cada mutante: "
    results['test_results'].select do |k,v|
      puts "La mutacion #{k} ha consistido en modificar la linea #{mutate_info[k]['num_line']}."
      print "La linea original era -> (#{mutate_info[k]['original_line'].strip}) "
      puts"y se ha mutado a -> (#{mutate_info[k]['mutate_line'].strip})"
      puts "Mutacion #{k} ha pasado: #{v['total_tests']}"
      puts "De los cuales ha obtenido el resultado esperado en #{v['passed']}"
      puts "Ha tenido #{v['failures']} fallos"
      puts "Ha tenido #{v['errors']} errores\n\n"
    end
    puts "\nFIN de resultados.\n\n"
  end

  def print_operation_add
    puts "Para agregar una nueva operacion introduzca los siguientes campos: nombre operacion, elemento actual, nuevo elemento para reemplazar, descripcion"
    puts "Escriba en el mismo orden pulsando enter tras cada elemento."
  end

  def print_repeated_name
    puts "Nombre de operacion ya existente, vuelva a intentarlo."
  end

  def print_empty_field
    puts "Campo vacio no valido. Vuelva a introducir un valor para el campo"
  end

  def print_csv_file_missing
    puts "El fichero de guardado y carga de operaciones no se encuentra en el dispositivo. Reinstale la aplicacion para solucionar el problema."
  end

  def print_select_create_or_use_or_delete
    print "Seleccione 1 si quiere crear una nueva operacion ,2 para usar una o 3 para eliminar una:"
  end

  def presentation
    puts "PROGRAMA DE MUTACIONES PARA RUBY."
    puts "  CREADO POR: IVÁN DÍAZ MORENO"
  end

  def selection
    print "Introduzca su seleccion: "
  end

  def remove_element
    print "Introduzca el nombre de la operacion que desea eliminar: "
  end

  def cant_remove
    puts "\n\nNo se puede eliminar una operacion no generada por el usuario o que no existe."
  end

  def can_remove
    puts "Operacion eliminada con exito."
  end

  def end_operand_selection
    puts "Para terminar de seleccionar introduzca 'fin' sin comillas.\n\n"
  end

  module_function :no_selection, :end_of_program, :no_mutations_generate, :transform_into_ast_error,
                  :print_mutations_and_tests_results, :print_operation_add, :print_repeated_name,
                  :print_empty_field, :print_csv_file_missing, :print_select_create_or_use_or_delete, :presentation,
                  :selection, :remove_element, :cant_remove, :end_operand_selection, :can_remove,
                  :original_file_and_test_errors
end
