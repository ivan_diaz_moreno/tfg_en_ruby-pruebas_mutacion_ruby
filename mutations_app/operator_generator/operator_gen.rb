require_relative '../config' # Import vars form config file
require_relative '../prints_module/print_module'
require_relative '../file_dir_manager/file_and_directory_manager'
require 'csv' # Import the content necessary to work with csv files

# This class contains all the functionality to load default operands in the system
# at the start of the application and generate new operands by the users.
class Operators
  include Prints
  extend  FileAndDirectoryManager
  # Hash which contains the operators of the system {"operator_to_replace": y, "new_operator": x}
  @@operators = Hash.new
  # Vars to save the pos of columns in the read csv
  @@name, @@operator, @@new_operator, @@description, @@operation_type = -1, -1, -1, -1, -1

  def initialize
    # Initializer of the class, build a new object which type is Operators
    begin
      load_default_operators
    rescue Exception => error
      print "Excepción capturada: ", error, "\n"
    end
  end

  def load_default_operators
    # Load the default operators located at /ini_operators_files/ini_operators.csv in the system
    # :return: None

    # First clear the hash with the actual operator to make a new load
    @@operators.clear
    path_csv_operators = OPERATORS_FILE_PATH + '/ini_operators_files/ini_operators.csv'
    # Check if the ini_file exists
    unless FileAndDirectoryManager.check_if_file_exists(path_csv_operators)
      Prints.print_csv_file_missing
      return
    end
    csv_content = CSV.read(path_csv_operators)
    # Skip the first row (csv_content.shift) because contains the header of the csv
    define_csv_cols(csv_content.shift)
    for operator in csv_content
      operator_name = operator[@@name]
      unless @@operators.key?(operator_name)
        @@operators[operator_name] = {"operator_to_replace" => operator[@@operator],
                                      "new_operator" => operator[@@new_operator],
                                      "type" => operator[@@operation_type],
                                      "description" => operator[@@description]}
      else
        raise Exception, "ERROR: Operator with same name already defined.", caller
      end
    end
  end

  def define_csv_cols(name_cols)
    # Find which element are in each column in the .csv
    # :param name_cols: name of the cols in the csv file
    # :return: None
    pos = [4, 3, 2, 1, 0]
    for col in name_cols
      if col == 'name'
        @@name = pos.pop
      elsif col == 'operator'
        @@operator = pos.pop
      elsif col == 'description'
        @@description = pos.pop
      elsif col == 'operation_type'
        @@operation_type = pos.pop
      else
        @@new_operator = pos.pop
      end
    end
  end

  def operators
    # Return the value of this variable
    @@operators
  end

  def add_new_operator
    # Allows the user to add a new operation to the system
    # :return: None
    num_of_selections = 4
    new_row = ""

    Prints.print_operation_add
    while num_of_selections > 0
      Prints.selection
      selection = gets.chomp # Chomp to not take the /n
      if num_of_selections == 4
        # Check the name of the new operation is not in the actual operations hash
        if not @@operators.key?(selection) && selection != ""
          num_of_selections -= 1
          new_row = "\"" + selection + "\""
        else
          Prints.print_repeated_name
        end
      else
        # Before the user put the description we need to save the operation type
        # R_U is a replacement operation created by user
        if num_of_selections == 1
          num_of_selections -= 1
          new_row = new_row + ",\"R_U\"," + "\"" +selection +"\""
        elsif selection != ""
          num_of_selections -= 1
          new_row = new_row + ',' + "\"" +selection +"\""
        else
          Prints.print_empty_field
        end
      end
    end
    # Add the line break
    new_row = new_row + "\n"
    path_csv_operators = OPERATORS_FILE_PATH + '/ini_operators_files/ini_operators.csv'
    # Check if the ini_file exists
    unless FileAndDirectoryManager.check_if_file_exists(path_csv_operators)
      Prints.print_csv_file_missing
      return
    end
    # Add the new row
    File.open(path_csv_operators, 'a+') {|f| f.write(new_row)}
    # Call load operators, to reload the actual execution with the new data
    load_default_operators
  end

  def remove_operator
    # Allows the user to remove a operation that was created by a user
    # :return: None
    Prints.remove_element
    selection = gets.chomp # Chomp to not take the /n

    path_csv_operators = OPERATORS_FILE_PATH + '/ini_operators_files/ini_operators.csv'
    # Check if the ini_file exists
    unless FileAndDirectoryManager.check_if_file_exists(path_csv_operators)
      Prints.print_csv_file_missing
      return
    end

    table = CSV.table(path_csv_operators)
    original_num_of_rows = CSV.foreach(path_csv_operators, headers: false).count

    table.delete_if do |row|
      row[:name] == selection && row[:operation_type] == 'R_U'
    end

    File.open(path_csv_operators, 'w') do |f|
      # Write the header without quotes
      f.write(table.headers.to_csv)
    end

    # Add the rest of the content with quotes
    File.open(path_csv_operators, 'a+') do |f|
      f.write(table.to_csv(write_headers: false, :quote_char=>'"', :force_quotes => true))
    end

    new_num_of_rows = CSV.foreach(path_csv_operators, headers: false).count

    # If now there are less rows, that means that the operator was deleted
    if original_num_of_rows > new_num_of_rows
      Prints.can_remove
      # Call load operators, to reload the actual execution with the new data
      load_default_operators
    else # Else means that the row wasn't deleted
      Prints.cant_remove
    end
  end

  def to_s
    # Return the string with the loaded operators
    "Operadores cargados: #{@@operators}\n"
  end

  # Private methods
  private :define_csv_cols, :load_default_operators, :to_s
end
