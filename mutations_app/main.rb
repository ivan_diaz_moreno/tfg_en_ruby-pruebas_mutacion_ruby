require_relative 'operator_selector/operators_selector'
require_relative 'operator_generator/operator_gen'
require_relative 'ast_transformer/transformation_operations'
require_relative 'prints_module/print_module'
require_relative 'replacement_module/replacement_operations'
require "parser/current" # Import parser content

module MainApplication
  include TransformerModule
  include Prints
  include ReplacementModule
  application_status = true
  operator_generator = Operators.new
  operator = operator_generator.operators
  Prints.presentation
  while application_status == true
    selector = Selector.new(operator)
    selection = selector.select_apply_operations_or_create_operation_or_delete
    # If selection is true means that user wants to generate a operator
    if selection == 'create'
      operator_generator.add_new_operator
      #Load in the selector the new operators
      selector.load_actual_operators(operator_generator.operators)
    elsif selection == 'delete'
      selector.print_available_operators
      operator_generator.remove_operator
      #Load in the selector the new operators
      selector.load_actual_operators(operator_generator.operators)
    elsif selection == 'use'
      selector.print_available_operators
      Prints.end_operand_selection
      selections = selector.select_operator
      unless selections.empty?
        file_path = selector.set_input_file_folder
        test_path = selector.set_test_or_input_file_folder
        begin
          TransformerModule.read_file(file_path) # OJO: AHORA MISMO PASO UN ARCHIVO ESTANDAR, HAY QUE CAMBIAR PARA QUE SEA GENERICO
        rescue StandardError => error
          puts error
        else
          code_ast, code_source_buffer = TransformerModule.generate_ast_from_content_file
          if code_ast != nil
            # Ask the user to save the mutations
            selection_save = selector.select_save_mutations
            ReplacementModule.make_mutations(code_ast, selections, code_source_buffer, test_path, selection_save, file_path)
          else
            puts Prints.transform_into_ast_error
          end
        end
      else
        puts Prints.no_selection
      end
    end
    # After a operation the system ask the user if wants to continue or leave the application
    application_status = selector.select_exit_application
  end
  puts Prints.end_of_program
end