require 'set' # Import elements needed to work with sets
require_relative '../file_dir_manager/file_and_directory_manager'
# Contains all the functionality to show the operators loaded in the system
# and allows the users to select the operands that they want
class Selector
  extend  FileAndDirectoryManager

  def initialize(operators)
    # Initializer of the class, build a new object which type is Selector
    load_actual_operators(operators)
  end

  def load_actual_operators(operators)
    # Load in the selector the current operators in the
    # system
    # :param operators: Hash which contains the current
    # replace operations loaded in the system
    # :return: None
    # Contains the name of the operators in the system
    @@operators_names = operators.keys
    # Contains the collection of operators loaded in
    # the system
    @@operators_collection = operators
  end

  def print_available_operators
    # Shows in the screen the name of the current operations in the system
    # :return: None
    puts 'Lista de operaciones disponibles:'
    @@operators_names.each do |name|
      puts "Operacion: #{name}, Descripcion: #{@@operators_collection[name]['description']}"
    end
  end

  def select_operator
    # It captures by keyboard the names selected by the user of the operators
    # that he wants to apply.
    # :return: The operator sign associated with each name entered and the
    # new sign to replace.
    in_selection = true
    selections = Set.new # Is a set because you cant repeat names

    while in_selection
      print 'Introduzca el nombre de la operacion de remplazo deseada: '
      selection = gets.chomp # Chomp to not take the /n
      if selections.include?(selection)
        puts 'La operacion ya ha sido seleccionada anteriormente.'
      elsif @@operators_names.include?(selection)
        selections.add(selection)
        puts "Seleccion guardada con exito.\n"
      elsif selection == 'fin'
        in_selection = false
      else
        puts 'El nombre introducido no tiene coincidencias.'
      end
    end

    return get_operator_and_new_operator(selections)
  end

  def set_test_or_input_file_folder
    # It captures by keyboard the path which contains the tests and check if the path exists
    # :return: the path of the tests.
    print "Introduzca el path que contiene los tests: "
    selection = gets.chomp # Chomp to not take the /n
    if FileAndDirectoryManager.check_if_folder_exits(selection)
      # Check if the path not finish with / or \
      unless selection.match(/.*\/$/) and selection.match(/.*\\$/)
        selection = selection + '/'
      end
      # Replace \ with / , if selection not contains / the gsub return nil
      refactor_selection = selection.gsub! '\\', '/'
      if refactor_selection
        refactor_selection
      else
        selection
      end
    else
      puts 'El path introducido no existe, vuelve a introducirlo'
      set_test_or_input_file_folder()
    end
  end

  def set_input_file_folder
    # It captures by keyboard the path which contains the input file and check if the file exists
    # :return: the path of the tests.
    print "Introduzca el path del fichero que se desea mutar: "
    selection = gets.chomp # Chomp to not take the /n
    if FileAndDirectoryManager.check_if_file_exists(selection)
      # Replace \ with / , if selection not contains / the gsub return nil
      refactor_selection = selection.gsub! '\\', '/'
      if refactor_selection
        refactor_selection
      else
        selection
      end
    else
      puts 'El path introducido no existe, vuelve a introducirlo'
      set_input_file_folder()
    end
  end


  def get_operator_and_new_operator(selections)
    # Given a list of operator names defined in the application it
    # finds its associated sign and the sign by which it should
    # be replaced.
    # :param selections: list with the name of the operators.
    # :return: (Array) old sign and new sign associated with the past
    # names as parameter.
    operators_to_apply = [] # Equivalent of Array.new
    selections.each do |selection|
      operators_to_apply.append(@@operators_collection[selection])
    end
    return operators_to_apply
  end

  def select_exit_application
    # Ask the user if want to exit the app and change the application_state to false
    # if the user select 1 or true if select 2
    # :return: true if want to continue false if not.
    puts '¿Quiere realizar otra operacion o salir?'
    print 'Escriba 1 para continuar o 2 para salir: '
    selection = gets.chomp # Chomp to not take the /n
    result = check_between_two_options(selection,"1","2")
    if result != nil
      return result
    else
      puts "Seleccion no valida, vuelva a intentarlo.\n\n\n"
      select_exit_application
    end
  end

  def select_save_mutations
    # Ask the user if want to save the mutations after the application of the tests
    # :return:true if want to save or false if not.
    puts '¿Quiere guardar las mutaciones o solo desea tener los resultados de los tests?'
    print 'Escriba true para guardar o false para solo realizar los tests: '
    selection = gets.chomp # Chomp to not take the /n
    result = check_between_two_options(selection,"true","false")
    if result != nil
      return result
    else
      puts "Seleccion no valida, vuelva a intentarlo.\n\n\n"
      select_save_mutations
    end
  end

  def select_apply_operations_or_create_operation_or_delete
    # Ask the user if want to create a operator or use a created operator
    # :return: create if want to create or use if want to use. Could be
    # return 'delete' if want to delete
    Prints.print_select_create_or_use_or_delete
    selection = gets.chomp # Chomp to not take the /n
    if selection == '1'
      return 'create'
    elsif selection == '2'
      return 'use'
    elsif selection == '3'
      return 'delete'
    else
      puts "Seleccion no valida, vuelva a intentarlo.\n\n\n"
      select_apply_operations_or_create_operation_or_delete
    end
  end

  def check_between_two_options(selection,op1,op2)
    # Given a selection and two possible options, see which option is the selection and
    # returns true if it is the first and false if it is the second.
    # :param selection: element to check.
    # :param op1: option1.
    # :param op2: option2.
    # :return: returns true if it is the first and false if it is the second.
    if selection == op1
      return true
    elsif selection == op2
      return false
    else
      return nil
    end
  end

  # Private methods
  private :get_operator_and_new_operator, :check_between_two_options
end
