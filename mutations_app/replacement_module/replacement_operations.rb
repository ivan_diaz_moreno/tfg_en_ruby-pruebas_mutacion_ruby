require "parser/current" # Import parser content
require_relative '../ast_transformer/nodes_rewriter'
require_relative '../ast_transformer/transformation_operations'
require_relative '../config' # Import vars form config file
require_relative '../manage_test_mutations/mutations_and_tests_manager'
require_relative '../ast_transformer/operators_extra_checks'


module ReplacementModule
  # This module contains all the functions to make the mutations in the code and save it in files
  include FileAndDirectoryManager
  include MutationsAndTestsManager
  include ExtraChecks

  def make_mutations(ast, operators, code_source_buffer, test_path, selection_save, file_path)
    # Calls the mutation functions for each operator to replace in the original ast.
    # Once the mutations have been generated for an operator, before moving on to the next,
    # it calls the execution of tests and finally cleans the directory
    # where the generated mutations were saved.
    # :param ast: the ast of the code.
    # :param operators: the operators to modify in the ast.
    # :param code_source_buffer: A buffer with source code.
    # :param test_path: The path which contains the tests.
    # :param test_path: The path which contains the input file.
    # :param selection_save: a boolean to know if want to save the mutation after the testing or not.
    # :param file_path: path of the file with the original content to mutate
    # :return: None


    @n_mutation = 0 # Count the number of actual mutations
    @mutation_info = Hash.new # Save info about the which line is mutated in every mutation generated.

    # Before starting a new run, tests that could exist from a previous run are removed
    FileAndDirectoryManager.delete_folder(MUTATIONS_FILE_PATH_SAVE)
    # And clean the hash to save the lines that have been modified in every mutation
    @mutation_info.clear
    @n_mutation = 0 # cont to name the mutations file. Increases with a new mutation and reset to 0 with new operator
    @n_operator = 0 # cont to name the mutations file. Increases with a new mutation with a new operator
    original_ast = ast # Save the original ast to make all the changes
    ast_rewriter = RewriteTree.new
    operators.each do |operators|
      ast_rewriter.replace_current_operators(operators)
      explore_nodes_and_call_rewriter(code_source_buffer, ast, ast_rewriter, operators,file_path)
      code_mutation = ast_rewriter.rewrite(code_source_buffer, ast)
      check_original_and_new_code(code_mutation, code_source_buffer.source,file_path)
      # Execute tests
      MutationsAndTestsManager.execute_tests(operators, file_path ,test_path, @n_operator.to_s, @mutation_info)
      # Delete the mutations if the user does not want them
      unless selection_save
        FileAndDirectoryManager.delete_folder(MUTATIONS_FILE_PATH_SAVE)
      end
      # Upload the cont to give the correct name for the new mutations with the new operands
      @n_mutation = 0
      @n_operator += 1
    end
  end

  def explore_nodes_and_call_rewriter(code_source_buffer, ast, ast_rewriter, operators, file_path)
    # Loops through the children from the given node and calls the appropriate
    # rewriter to generate the mutation operation based on the type of node.
    # When the rewriter finishes, it checks if something
    # has been mutated with respect to the original code.
    # :param code_source_buffer: A buffer with source code.
    # :param ast: the actual node of the ast to star the loop.
    # :param ast_rewriter: a object of the type RewriteTree to call the correct rewrite
    # :param operators: dict with the actual operator to search and replace
    # :param file_path: path of the file with the original content to mutate
    # :return: None
    ast.children.each { |node|
      if node.is_a?(Parser::AST::Node)
        explore_nodes_and_call_rewriter(code_source_buffer, node, ast_rewriter, operators,file_path)
        code_mutation = ast_rewriter.rewrite(code_source_buffer, node)
        check_original_and_new_code(code_mutation, code_source_buffer.source,file_path)
        # Call checker to evaluate if this type of node has special features that require further changes
        generate_mut_array = ExtraChecks.check_type_of_node(node, ast_rewriter, code_source_buffer, operators)
        generate_mut_array.each { |code_mutation|
          check_original_and_new_code(code_mutation, code_source_buffer.source,file_path)
        }
      end
    }
  end

  def check_original_and_new_code(code_mutation, original_code,file_path)
    # Given two codes compare if are the same. If are different, then call to generate
    # a mutation file with the code_mutation. And save the line that has been changed
    # :param code_mutation: the str of the code mutate.
    # :param ast_rewriter: the str of the original code.
    # :param file_path: path of the file with the original content to mutate
    # :return: None
    if code_mutation != original_code
      delimiters = ["\n ", "\n\n", "\n", "\n\n "]
      separator = Regexp.union(delimiters)
      mutate_lines = code_mutation.split(separator).to_set
      original_lines = original_code.split(separator).to_set

      difference_line = (mutate_lines ^ original_lines).to_a
      # difference_line is a Set with the line mutate in pos 1 and original line in pos 0
      # Now check the line number of in the original code. Need to add one because
      # the array start at index 0 but the code file start in line 1, so the element in
      # the index 0 is the line in the row number 1 of the file
      # line_number = original_lines.to_a.find_index(difference_line[0]) + 1
      line_number = FileAndDirectoryManager.get_line_number(file_path, difference_line[0])
      difference_line.append(line_number)
      generate_mutation(code_mutation, difference_line)
    end
  end

  def generate_mutation(new_code, difference_line)
    # Given an input code, it generates a file with that content.
    # Check if the mutation directory currently exists and if it
    # does not exist generate it to save the new file.
    # :param new_code: the str of the code mutate.
    # :param difference_line: an array with 3 elements: the original line, the mutate line
    # and the number of the line in this order.
    # :return: None
    original_line_index , mutate_line_index, number_of_line_index = 0,1,2
    file_mutation_name = 'mutation'+ + @n_operator.to_s + "_" + @n_mutation.to_s + '.rb'
    file_path = MUTATIONS_FILE_PATH_SAVE + file_mutation_name
    # If the folder to save the mutations does not exist, then create it
    FileAndDirectoryManager.generate_folder(MUTATIONS_FILE_PATH_SAVE)
    FileAndDirectoryManager.with_open File, file_path, 'w' do |f|
      f.write(new_code)
    end
    # Save the info of which line was mutated to be used in the representation of results
    @mutation_info[file_mutation_name] = {
        'original_line' => difference_line[original_line_index],
        'mutate_line' => difference_line[mutate_line_index],
        'num_line' => difference_line[number_of_line_index]
    }
    @n_mutation += 1
  end

  module_function :make_mutations, :explore_nodes_and_call_rewriter, :generate_mutation,
                  :check_original_and_new_code
end
