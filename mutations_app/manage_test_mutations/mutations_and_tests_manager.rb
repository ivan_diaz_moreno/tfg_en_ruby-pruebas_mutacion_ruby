require_relative '../config' # Import vars form config file
require_relative '../manage_test_mutations/evaluate_test_results'

module MutationsAndTestsManager
  # This module contains all the functionality to manage the mutations and execute the tests in this
  # mutations and work with the results
  include FileAndDirectoryManager
  include Prints

  def execute_tests(operators, file_path ,test_path, num_of_actual_operator, mutate_info)
    # Take the mutations generated to the last operator mutation and copy the info in the file given by
    # the file_path. This file must be the file referenced in the tests. Then generate a all.rb file
    # with all the tests and execute it and then save the results in a file results.txt.
    # Then call the test evaluator to evaluate the results and print it.
    # :param operators: actual operators
    # :param file_path: file path with the original content file without mutations and which is referenced from tests.
    # :param test_path: path with all the tests
    # :param num_of_actual_operator: str the number which contains the files name of mutations
    # :param mutate_info: Dictionary whose keys are the name of the file that contains the mutant and its value
    # is a dictionary that stores: the content of the original line, the content of the mutated line,
    # the number of the line in the file that has been mutated.
    # with the actual operator. Ex: if is the second operator, the name will be mutation2_x.rb and
    # the num_of_actual_operator must be 2
    # :return: None

    # Check if exist the mutations folder, this folder only exist if there are mutations
    if FileAndDirectoryManager.check_if_folder_exits(MUTATIONS_FILE_PATH_SAVE)
      @test_result_path = TESTS_RESULTS_FILE_PATH_SAVE
      FileAndDirectoryManager.generate_folder(@test_result_path)
      # Generate the test results evaluator
      @test_evaluator = TestResultsEvaluator.new(@test_result_path)
      # Save the original content of the input file
      FileAndDirectoryManager.with_open File, file_path, 'r' do |f|
        @original_content = f.read
      end
      # Generate a all.rb file with all the tests to execute only one command to run all the tests
      generate_global_rb_test_file(test_path)
      test_file_path = test_path + 'all.rb'
      # First check if the tests pass with the original file
      if execute_test_for_original_file(test_file_path) == false
        # If the results have errors
        Prints.original_file_and_test_errors
        # Delete the file with the exit results of the tests
        FileAndDirectoryManager.delete_folder(@test_result_path)
        # And delete the all.rb with all the tests calls
        FileAndDirectoryManager.delete_file(test_file_path)
        return
      end

      FileAndDirectoryManager.get_file_names_from_folder(MUTATIONS_FILE_PATH_SAVE).each do |file|
        # The program checks that the file name matches only with the filenames of the last mutation generation.
        # This check is needed because in the path could be mutation files for the prev operators and we dont
        # want to execute the tests in this old mutations.
        if file.match("mutation#{num_of_actual_operator}_[0-9]*")
          mutation_file_path = MUTATIONS_FILE_PATH_SAVE + "/" + file
          # Prepare the original file with the mutation content
          FileAndDirectoryManager.with_open File, mutation_file_path, 'r' do |f|
            @mutation_content = f.read
          end
          FileAndDirectoryManager.with_open File, file_path, 'w' do |f|
            f.write(@mutation_content)
          end
          # Execute tests from the generate file 'all.rb' with al the tests
          # The option --stop-on-failure stop when the first test fail and write the results in the file
          # `ruby #{test_file_path} --stop-on-failure > #{@test_result_path}results_test.txt`.chomp
          `ruby #{test_file_path} > #{@test_result_path}results_test.txt`.chomp
          # Call the evaluator to check the results
          @test_evaluator.evaluate_results()
          # When all the tests are passed to the mutation, the evaluator update the data
          @test_evaluator.update_results(file)
        end
      end
      #Call the evaluator asking for the results
      results = @test_evaluator.get_results_for_all_mutations_and_tests
      Prints.print_mutations_and_tests_results(results, operators, mutate_info)
      # When all the tests and mutations are passed then the original file take its original content
      FileAndDirectoryManager.with_open File, file_path, 'w' do |f|
        f.write(@original_content)
      end
      # Delete the file with the exit results of the tests
      FileAndDirectoryManager.delete_folder(@test_result_path)
      # And delete the all.rb with all the tests calls
      FileAndDirectoryManager.delete_file(test_path+'all.rb')
    else
      puts Prints.no_mutations_generate(operators)
    end
  end

  def execute_test_for_original_file(test_file_path)
    # Execute test and call to evaluate if the results dont have errors, failures
    # pendings, ommissions or notifications
    # :param test_file_path: path which contains the test all.rb
    # :return: true if dont have bad elements, false if have
    `ruby #{test_file_path} > #{@test_result_path}results_test.txt`.chomp
    # Call the evaluator to check the results
    return @test_evaluator.evaluate_not_bad_exit_in_test_results
  end

  def generate_global_rb_test_file(test_path)
    # Given a test_path with tests, generate a all.rb file with all the require tests in the folder.
    # This file all.rb could be called to execute in one command all the tests
    # :param test_path: path which contains the tests
    # :return: None
    FileAndDirectoryManager.with_open File, test_path+'all.rb', 'a' do |f|
      FileAndDirectoryManager.get_file_names_from_folder(test_path).each do |test|
        if test != "all.rb"
          test_file_path = test_path + test
          f.write("require \"#{test_file_path}\"\n")
        end
      end
    end
  end

  module_function :execute_tests, :generate_global_rb_test_file, :execute_test_for_original_file
end
