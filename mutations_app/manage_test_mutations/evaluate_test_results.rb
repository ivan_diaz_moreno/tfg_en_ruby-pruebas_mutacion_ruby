require_relative '../file_dir_manager/file_and_directory_manager'

class TestResultsEvaluator
  extend FileAndDirectoryManager

  def initialize(result_file_path)
    # Initializer of the class, build a new object which type is TestResultsEvaluator
    @@test_result_path = result_file_path
    @@alive_mutations = 0
    @@dead_mutations = 0
    @@total_mutations = 0
    @@failures_for_mutation = 0
    @@errors_for_mutation = 0
    @@total_tests_for_mutation = 0
    @@mutations_status = Hash.new
    @@test_results = Hash.new
  end

  def evaluate_results
    # Read the file with the results data and evaluate if the mutation pass the tests.
    # Call update_results to update the data in the evaluator obj
    # :return: None

    position_line_with_info = -3 #The line is 3 lines before the end of the file always
    FileAndDirectoryManager.with_open File, @@test_result_path + 'results_test.txt', 'r' do |f|
      @info_lines = f.readlines
    end
    #Get the line with the information of which tests passed, how many failures are ...
    results_line = @info_lines[position_line_with_info]
    results = results_line.split(',')
    # Take the number of execute tests, the number of errors and failures
    results.each do |result|
      if result.match('.+[0-9]* failures.*')
        @@failures_for_mutation = result.delete(" failures").to_i
      elsif result.match('.+[0-9]* errors.*')
        @@errors_for_mutation = result.delete(" errors").to_i
      elsif result.match('.+[0-9]* tests.*')
        @@total_tests_for_mutation = result.delete(" tests").to_i
      end
    end
  end

  def update_results(mutation_file_name)
    # Update the number of mutations that pass the test or not and the
    # total number of mutations
    # :param mutation_file_name: mutation which passed the tests and
    # which results are in the results_test.txt
    # :return: None
    @@total_mutations += 1
    if @@errors_for_mutation == 0 && @@failures_for_mutation == 0
      @@alive_mutations += 1
      status = 'alive'
    else
      @@dead_mutations += 1
      status = 'dead'
    end
    @@mutations_status[mutation_file_name] = status
    @@test_results[mutation_file_name] = {
        'total_tests' => @@total_tests_for_mutation,
        'failures' => @@failures_for_mutation,
        'errors' => @@errors_for_mutation,
        'passed' => @@total_tests_for_mutation - (@@failures_for_mutation + @@errors_for_mutation)
    }
  end

  def evaluate_not_bad_exit_in_test_results
    # Evaluate if there aren't fails , errors, pendings, omissions or notifications in the results
    # of a test.
    # :return: true if there aren't, false if there are
    position_line_with_info = -3 #The line is 3 lines before the end of the file always
    FileAndDirectoryManager.with_open File, @@test_result_path + 'results_test.txt', 'r' do |f|
      @info_lines = f.readlines
    end
    #Get the line with the information of which tests passed, how many failures are ...
    results_line = @info_lines[position_line_with_info]
    if results_line.match('^.*, 0 failures, 0 errors, 0 pendings, 0 omissions, 0 notifications.*')
      return true
    else
      return false
    end
  end

  def get_results_for_all_mutations_and_tests
    # Return the mutations status, the number of alive mutations, the number of died mutations and
    # the mutation percentage evaluating the data
    # in the evaluator class
    # :return: the mutations status, the number of alive mutations,
    # the number of died mutations and the mutation percentage

    if @@total_mutations != 0
      mutation_percentage = (@@dead_mutations / @@total_mutations) * 100
    else
      mutation_percentage = 0
    end

    return {'mutations_status' => @@mutations_status,
            'alive_mutations' => @@alive_mutations,
            'dead_mutations' => @@dead_mutations,
            'mutation_percentage' => mutation_percentage,
            'mutations_status' => @@mutations_status,
            'test_results' => @@test_results}
  end
end
